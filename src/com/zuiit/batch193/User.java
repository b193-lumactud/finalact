package com.zuiit.batch193;

public class User {

    private String firstName;
    private String lastName;
    private int age;
    private String address;

    //empty constructor
    public User(){};
    //parameterized constructor
    public User (String firstName, String lastName, String address, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.age = age;
    }

    //getters
    public String getFirstName(){
        return this.firstName;
    }
    public String getLastName() {
        return this.lastName;
    }
    public int getAge(){
        return this.age;
    }
    public String getAddress() {
        return this.address;
    }

    //setters
    public String setFirstName(String firstName){
        return this.firstName = firstName;
    }

    public String setLastName(String lastName){
        return this.lastName = lastName;
    }

    public int setAge(int age) {
        return this.age = age;
    }

    public String setAddress(String address) {
        return this.address = address;
    }

}
