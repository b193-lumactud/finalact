package com.zuiit.batch193;

public class Main {
    public static void Main (String[] args) {
        User newUser = new User();
        System.out.println("User's First Name: "+newUser.setFirstName("Jelly"));
        System.out.println("User's Last Name: "+newUser.setLastName("Lumactud"));
        System.out.println("User's Age: "+newUser.setAge(29));
        System.out.println("User's Address: "+newUser.setAddress("General Trias, Cavite"));

        Course newCourse = new Course();
        System.out.println("Course's Name: "+newCourse.setName("Physics 101"));
        System.out.println("Course's Description: "+newCourse.setDescription("Learn Physics"));
        System.out.println("Course's Seats: "+newCourse.setSeats(30));
        System.out.println("Course's Fee: "+newCourse.setFee(1000.25));

        newCourse.teach();


    }
}
