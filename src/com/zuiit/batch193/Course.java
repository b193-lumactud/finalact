package com.zuiit.batch193;

public class Course{

    private String name;
    private String description;
    private int seats;
    private double fee;
    private String startDate;
    private String endDate;
    private Instructor i;

    public Course(String name, String description, int seats, double fee, String startDate, String endDate) {
        this.name = name;
        this.description = description;
        this.seats = seats;
        this.fee = fee;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Course() {
        this.i = new Instructor("Gino");
    }


    //getters
    public String getInstructorName(){
        return this.i.getName();
    }
    public String getName(){ return this.name;}
    public String getDescription(){return this.description;}
    public int getSeats() { return this.seats;}
    public double getFee() {return this.fee;}
    public String getStartDate() {return this.startDate;}
    public String getEndDate() {return this.endDate;}
    //setters
    public String setName(String name){
        return this.name = name;
    }
    public String setDescription(String description){return this.description = description;}
    public int setSeats(int seats){return this.seats = seats;}
    public double setFee(double fee){return this.fee = fee;}
    public String setStartDate(String startDate){return startDate = startDate;}
    public String setEndDate(String endDate){return endDate = endDate;}

    public void teach(){
        System.out.println("Course instructor's first name: "+getInstructorName());
    }

}
